import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { routerReducer, RouterReducerState, RouterState, StoreRouterConnectingModule } from '@ngrx/router-store';
import { RouterEffects } from './core/store/router/router.effects';
import { RouterSerializer } from './core/store/router/router.serializer';
import { authReducer, AuthState } from './core/store/auth/auth.reducer';
import { AuthEffects } from './core/store/auth/auth.effects';
import { AuthInterceptor } from './core/store/auth/auth.interceptor';
import { IfLoggedDirective } from './core/store/auth/if-logged.directive';

export interface AppState {
  // NEW
  authentication: AuthState;
  router: RouterReducerState;
}
export const reducers: ActionReducerMap<AppState> = {
  authentication: authReducer,
  router: routerReducer
};

@NgModule({
  declarations: [
    AppComponent,
    IfLoggedDirective,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    StoreModule.forRoot(reducers),
    StoreDevtoolsModule.instrument({
      maxAge: 12
    }),
    EffectsModule.forRoot([RouterEffects, AuthEffects]),
    StoreRouterConnectingModule.forRoot({
      routerState: RouterState.Minimal,
      // NEW: custom serializer
      serializer: RouterSerializer,
    }),

  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
