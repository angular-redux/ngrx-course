// app.component.ts
import { Component, ViewChild } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { AppState } from './app.module';
import { logout } from './core/store/auth/auth.actions';
import { Observable } from 'rxjs';
import { getIsLogged } from './core/store/auth/auth.selectors';

@Component({
  selector: 'app-root',
  template: `
    <button routerLink="items" *appIfLogged>items</button>
    <button routerLink="pexels">Pexels Photo</button>
    <button routerLink="pexels-video">Pexels Video</button>
    <button (click)="logoutHandler()" *appIfLogged>Logout</button>
    <hr>
    <router-outlet></router-outlet>
  `,
})
export class AppComponent {
  isLogged$: Observable<boolean> = this.store.pipe(select(getIsLogged))
  constructor(private store: Store<AppState>) { }

  logoutHandler(): void {
    this.store.dispatch(logout())
  }
}
