import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './core/store/auth/auth.guard';

const routes: Routes = [
  { path: 'pexels', loadChildren: () => import('./features/pexels-photo/pexels-photo.module').then(m => m.PexelsPhotoModule) },
  { path: 'items', canActivate: [AuthGuard], loadChildren: () => import('./features/items/items.module').then(m => m.ItemsModule) },
  { path: 'pexels-video', loadChildren: () => import('./features/pexels-video/pexels-video.module').then(m => m.PexelsVideoModule) },
  { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
  { path: '**', redirectTo: 'pexels'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
