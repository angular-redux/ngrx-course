import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PexelsVideoRoutingModule } from './pexels-video-routing.module';
import { PexelsVideoComponent } from './pexels-video.component';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { PexelsVideoSearchEffects } from './store/search/pexels-video-search.effects';
import { PexelsVideoSearchState, searchReducer } from './store/search/pexels-video-search.reducers';
import { PexelsPlayerState, playerReducer } from './store/player/player.reducer';
import { filtersReducer, PexelsFilterState } from './store/filters/pexels-video-filters.reducer';

export interface PexelsVideoState {
  search: PexelsVideoSearchState;
  player: PexelsPlayerState;
  // NEW
  filters: PexelsFilterState;
}
export const reducers: ActionReducerMap<PexelsVideoState> = {
  search: searchReducer,
  player: playerReducer,
  // NEW
  filters: filtersReducer
};

@NgModule({
  declarations: [PexelsVideoComponent],
  imports: [
    CommonModule,
    PexelsVideoRoutingModule,
    StoreModule.forFeature('pexels-video', reducers),
    EffectsModule.forFeature([PexelsVideoSearchEffects])
  ]
})
export class PexelsVideoModule { }
