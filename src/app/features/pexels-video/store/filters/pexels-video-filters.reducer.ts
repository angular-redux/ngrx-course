import { createReducer, on } from '@ngrx/store';
import { filterByMinResolution } from './pexels-video-filters.actions';

export interface PexelsFilterState {
  minResolution: number;
}

export const initialState: PexelsFilterState = {
  minResolution: 0,
};

export const filtersReducer = createReducer(
  initialState,
  on(filterByMinResolution, (state, action) => ({ minResolution: action.minResolution})),
);
