import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PexelsVideoState } from '../../pexels-video.module';

export const getItemFeature = createFeatureSelector<PexelsVideoState>('pexels-video');

export const getCurrentVideo = createSelector(getItemFeature, state => state.player.currentVideo);
export const getCurrentVideoUrl = createSelector(getItemFeature, state => state.player.currentVideo?.video_files[0].link);

