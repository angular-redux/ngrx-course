import { createReducer, on } from '@ngrx/store';
import {  showVideo } from './player.actions';
import { Video } from '../../model/pexels-video-response';
import { searchVideos } from '../search/pexels-video-search.actions';

export interface PexelsPlayerState {
 currentVideo: Video;
}

export const initialState: PexelsPlayerState = {
  currentVideo: null,
};

export const playerReducer = createReducer(
  initialState,
  on(searchVideos, (state) => ({ ...state, currentVideo: null})),
  on(showVideo, (state, action) => ({ ...state, currentVideo: action.video})),
);
