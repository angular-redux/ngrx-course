import { createReducer, on } from '@ngrx/store';
import { searchVideos, searchVideosFails, searchVideosSuccess } from './pexels-video-search.actions';
import { Video } from '../../model/pexels-video-response';

export interface PexelsVideoSearchState {
  list: Video[];
  hasError: boolean;
  pending: boolean;
}

export const initialState: PexelsVideoSearchState = {
  list: [],
  hasError: false,
  pending: false,
};

export const searchReducer = createReducer(
  initialState,
  on(searchVideos, (state, action) => ({ ...state, pending: true, hasError: false})),
  on(searchVideosSuccess, (state, action) => ({ list: action.items, hasError: false, pending: false})),
  on(searchVideosFails, (state, action) => ({ ...state, hasError: true, pending: false})),
);
