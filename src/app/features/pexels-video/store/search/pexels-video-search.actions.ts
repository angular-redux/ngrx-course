import { createAction, props } from '@ngrx/store';
import { Video } from '../../model/pexels-video-response';


export const searchVideos = createAction(
  '[pexels-video] Search',
  props<{ text: string }>()
);

export const searchVideosSuccess = createAction(
  '[pexels-video] Search Success',
  props<{ items: Video[] }>()
);

export const searchVideosFails = createAction(
  '[pexels-video] Search Fails',
);


