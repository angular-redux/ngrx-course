import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PexelsVideoState } from '../../pexels-video.module';
import { Video } from '../../model/pexels-video-response';
import { getCurrentVideo } from '../player/player.selectors';

export const getItemFeature = createFeatureSelector<PexelsVideoState>('pexels-video');

// OLD
// export const getPexelsVideo = createSelector(getItemFeature, state => state.search.list);
// NEW
export const getPexelsVideo = createSelector(
  getItemFeature, state => state.search.list.filter(item => item.width > state.filters.minResolution)
);
export const getPexelsVideoSearchPending = createSelector(getItemFeature, state => state.search.pending);
export const getPexelsVideoSearchError = createSelector(getItemFeature, state => state.search.hasError);

export const getPexelsVideoPictures =
  createSelector(
    getCurrentVideo,
    getPexelsVideo,
    (currentVideo: Video, videos: Video[]) => {
        return videos.find(v => v.id === currentVideo?.id)?.video_pictures
          .map(image => image.picture);
      }
    );
