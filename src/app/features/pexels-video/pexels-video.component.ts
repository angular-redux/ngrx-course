import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { PexelsVideoState } from './pexels-video.module';
import { searchVideos } from './store/search/pexels-video-search.actions';
import { Observable } from 'rxjs';
import { Video } from './model/pexels-video-response';
import { getPexelsVideo, getPexelsVideoPictures, getPexelsVideoSearchPending } from './store/search/pexels-video-search.selectors';
import { showVideo } from './store/player/player.actions';
import { getCurrentVideoUrl } from './store/player/player.selectors';
import { filterByMinResolution } from './store/filters/pexels-video-filters.actions';

@Component({
  selector: 'app-pexels-video',
  template: `
    <input type="text" (keydown.enter)="searchVideoHandler($event)">
    <div *ngIf="videosSearchPending$ | async">loading...</div>

    <div *ngIf="getCurrentVideoUrl$ | async as video">
      <video width="100%" [src]="video" controls>
        Your browser does not support the video tag.
      </video>
    </div>

    <div
      *ngIf="videoPictures$ | async as videoPictures"
      style="display: flex; overflow: auto; width: 100%"
    >
      <img
        [src]="picture" alt="" width="100"
        *ngFor="let picture of videoPictures"
      >
    </div>

    <hr>
    <!--NEW-->
    <select (change)="filterByResolution($event)">
      <option>0</option>
      <option>2000</option>
      <option>3000</option>
      <option>4000</option>
    </select>

    <div *ngFor="let video of videos$ | async" style="display: flex; flex-wrap: wrap">
      <img [src]="video.image" alt="" width="100" (click)="showPreview(video)">
      {{video.width}}x{{video.height}}
    </div>
  `,
})
export class PexelsVideoComponent implements OnInit {
  videos$: Observable<Video[]> = this.store.pipe( select (getPexelsVideo));
  videosSearchPending$: Observable<boolean> = this.store.pipe( select (getPexelsVideoSearchPending));
  videoPictures$: Observable<string[]> =  this.store.pipe(select(getPexelsVideoPictures));
  getCurrentVideoUrl$: Observable<string> =  this.store.pipe(select(getCurrentVideoUrl));

  constructor(private store: Store<PexelsVideoState>) { }

  ngOnInit(): void {
    this.store.dispatch(searchVideos({ text: 'nature' }));
  }

  searchVideoHandler(event: KeyboardEvent): void {
    this.store.dispatch(searchVideos({ text: (event.target as HTMLInputElement).value}));
  }

  showPreview(video: Video): void {
    this.store.dispatch(showVideo({ video }));
  }

  // NEW
  filterByResolution(event: Event): void {
    const minResolution = +(event.target as HTMLSelectElement).value;
    this.store.dispatch(filterByMinResolution({ minResolution }));
  }

}
