import { Component } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { Credentials } from '../../core/store/auth/auth';
import { login } from '../../core/store/auth/auth.actions';
import { getAuthError } from '../../core/store/auth/auth.selectors';

@Component({
  selector: 'app-login',
  template: `
    <form #f="ngForm" (submit)="signin(f.value)" class="container mt-5" style="max-width: 400px;">
      <h2>Login</h2>

      <div *ngIf="(authError$ | async)">Wrong credentials</div>

        <input type="text"
           placeholder="email"
           [ngModel] name="email"  required>
        <input
          type="password"
          placeholder="password" [ngModel]
          name="password" required>

      <button type="submit">SIGN IN</button>
    </form>

  `,
  styles: []
})
export class LoginComponent {
  authError$ = this.store.pipe(select(getAuthError));

  constructor(private store: Store<AppState>) {}

  signin(formData: Credentials): void {
    const { email, password } = formData;
    this.store.dispatch(login({email, password}));
  }
}
