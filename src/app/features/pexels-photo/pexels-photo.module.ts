import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PexelsPhotoComponent } from './pexels-photo.component';
import { PexelsPhotoRoutingModule } from './pexels-photo-routing.module';

@NgModule({
  declarations: [PexelsPhotoComponent],
  exports: [
    PexelsPhotoComponent
  ],
  imports: [
    CommonModule,
    PexelsPhotoRoutingModule
  ]
})
export class PexelsPhotoModule { }
