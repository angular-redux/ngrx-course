import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PexelsPhotoComponent } from './pexels-photo.component';


const routes: Routes = [{ path: '', component: PexelsPhotoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PexelsPhotoRoutingModule { }

