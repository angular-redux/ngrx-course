import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { PexelsPhotoStore } from './local-store/pexels-photo-store.service';

@Component({
  selector: 'app-pexels',
  template: `
    <br>
    <!--NEW-->
    <button (click)="search('landscape')">landscape</button>
    <button (click)="search('people')">people</button>
    <button (click)="reset()">reset</button>
    <!--NEW-->
    <select (change)="filterByResolution($event)">
      <option>0</option>
      <option>3000</option>
      <option>5000</option>
    </select>

    <li *ngFor="let photo of photos$ | async">
      <img [src]="photo.src.tiny" alt="" width="100">
      <div>{{photo.width}}x{{photo.height}}</div>
    </li>
  `,
  providers: [PexelsPhotoStore]
})
export class PexelsPhotoComponent implements OnInit {
  photos$ = this.pexelsStore.photos$;

  constructor(
    public readonly pexelsStore: PexelsPhotoStore
  ) {}

  ngOnInit(): void {
    this.search('nature');
  }

  search(text: string): void {
    this.pexelsStore.search(text);
  }

  reset(): void {
    this.pexelsStore.searchReset();
  }

  filterByResolution(event: Event): void {
    // NEW 1: manual setState
    // this.pexelsStore.setState(s => ({ ...s, minResolution: event.target.value}))
    // NEW 2: invoke store method
    this.pexelsStore.filterByResolution(+(event.target as HTMLSelectElement).value);
  }
}
