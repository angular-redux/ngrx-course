import { Injectable } from '@angular/core';
import { PexelsPhotoResponse } from '../model/pexels';
import { ComponentStore } from '@ngrx/component-store';
import { catchError, switchMap, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { EMPTY, Observable } from 'rxjs';

// UPDATE
export interface PexelsPhotoState {
  data: PexelsPhotoResponse;
  minResolution: number;
}

// UPDATE
const INITIAL_STATE = {
  data: {  photos: [] },
  minResolution: 0
} as PexelsPhotoState;

@Injectable()
export class PexelsPhotoStore extends ComponentStore<PexelsPhotoState> {

  constructor(private http: HttpClient) {
    super(INITIAL_STATE);
  }

  // NEW
  readonly photos$ = this.select(state => state.data.photos
    .filter(photo => photo.width > state.minResolution)
  );

// Each new call of getMovie(id) pushed that id into searchText$ stream.
  readonly search = this.effect((searchText$: Observable<string>) => {
    return searchText$.pipe(
      switchMap((text) => this.http.get<PexelsPhotoResponse>(
        'https://api.pexels.com/v1/search?per_page=10&query=' + text,
        { headers: { Authorization: '563492ad6f9170000100000189ac030285b04e35864a33b95c2838be' } }
      ).pipe(
        tap({
          next: (pexelsResponse) => this.searchSuccess(pexelsResponse),
          error: (e) => console.log(e),
        }),
        // 👇 Handle potential error within inner pipe.
        catchError(() => EMPTY),
      )),
    );
  });

  readonly searchSuccess = this.updater((state, response: PexelsPhotoResponse) => ({
    // UPDATE
    ...state,
    data: {...response},
  }));

  readonly searchReset = this.updater((state) => {
    return INITIAL_STATE;
  });

  // NEW
  readonly filterByResolution = this.updater((state, minResolution: number) => {
    return {
      ...state,
      minResolution
    };
  });

}
