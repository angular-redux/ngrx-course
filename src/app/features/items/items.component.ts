import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';
import { Item } from '../../model/item';
import { ItemsState } from './items.module';
import { getItems, getItemsErrors } from './store/items.selectors';
import { addItem, addItemSuccess, deleteItem, loadItems } from './store/items.actions';
import { go } from '../../core/store/router/router.actions';

@Component({
  selector: 'app-items',
  template: `
    <!--NEW-->
    <div  *ngIf="hasError$ | async">Server error</div>

    <form #f="ngForm" (submit)="addItemHandler(f)" >
      <input type="text" [ngModel] name="name">
    </form>

    <li *ngFor="let item of items$ | async">
      {{item.name}}
      <button (click)="deleteHandler(item.id)">Delete</button>
    </li>
  `,
  styles: [
  ]
})
export class ItemsComponent {

  @ViewChild('f') form: NgForm;
  items$: Observable<Item[]> = this.store.pipe(select(getItems));
  // NEW
  hasError$: Observable<boolean> = this.store.pipe(select(getItemsErrors));

  constructor(
    private store: Store<ItemsState>,
    private actions: Actions
  ) {
    this.store.dispatch(loadItems());
    actions
      .pipe(
        ofType(addItemSuccess)
      )
      .subscribe(() => {
        this.form.reset();
      });

    /*setTimeout(() => {
      store.dispatch(go({ path: 'pexels-video'}))
    }, 1000);*/
  }

  deleteHandler(id: number): void {
    this.store.dispatch(deleteItem({ id }));
  }

  addItemHandler(form: NgForm): void {
    const item: Item = {
      ...form.value
    };
    this.store.dispatch(addItem({ item }));

    // UPDATED
    // form.reset()
  }

}
