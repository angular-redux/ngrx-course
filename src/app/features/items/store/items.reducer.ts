import { createReducer, on } from '@ngrx/store';
import { Item } from '../../../model/item';
import {
  addItemFail,
  addItemSuccess,
  deleteItemFail,
  deleteItemSuccess, loadItems,
  loadItemsFail,
  loadItemsSuccess
} from './items.actions';

// UPDATE
export interface UsersState {
  hasError: boolean;
  list: Item[];
}

// UPDATE
export const initialState: UsersState = {
  hasError: false,
  list: []
};

// UPDATE
export const reducer = createReducer(
  initialState,
  on(loadItems, (state) => ({ ...state, hasError: false})),
  on(loadItemsSuccess, (state, action) => ({list: [...action.items], hasError: false})),
  on(loadItemsFail, (state) => ({ ...state, hasError: true})),

  // on(deleteItem, (state) => ({ ...state, hasError: true})),
  on(deleteItemSuccess, (state, action) => ({ list: state.list.filter(item => item.id !== action.id), hasError: false})),
  on(deleteItemFail, (state) => ({ ...state, hasError: true})),

  // on(addItem, (state) => ({ ...state, hasError: true})),
  on(addItemSuccess, (state, action) => ({ list: [...state.list, action.item], hasError: false})),
  on(addItemFail, (state) => ({ ...state, hasError: true})),
);
