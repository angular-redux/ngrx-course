// app/store/items.selectors.ts
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ItemsState } from '../items.module';

export const getItemFeature = createFeatureSelector<ItemsState>('items');

export const getItems = createSelector(getItemFeature, state => state.items.list);
export const getItemsErrors = createSelector(getItemFeature, state => state.items.hasError);
