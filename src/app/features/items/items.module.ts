import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ItemsRoutingModule } from './items-routing.module';
import { ItemsComponent } from './items.component';
import { reducer, UsersState } from './store/items.reducer';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { FormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { ItemsEffects } from './store/items.effects';

export interface ItemsState {
  items: UsersState;        // instead of items: Items[]
}
export const reducers: ActionReducerMap<ItemsState> = {
  items: reducer
};

@NgModule({
  declarations: [ItemsComponent],
  imports: [
    CommonModule,
    ItemsRoutingModule,
    FormsModule,
    StoreModule.forFeature('items', reducers),
    EffectsModule.forFeature([ItemsEffects])
  ]
})
export class ItemsModule { }
