// app/core/auth/auth.ts
export interface Auth {
  token: string;
  role: string;
  displayName: string;
}

export interface Credentials {
  email: string;
  password: string;
}
