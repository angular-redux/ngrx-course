import { Directive, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { distinctUntilChanged, filter, map, takeUntil, tap } from 'rxjs/operators';
import { AppState } from '../../../app.module';
import { getIsLogged } from './auth.selectors';

@Directive({
  selector: '[appIfLogged]'
})
export class IfLoggedDirective implements OnInit, OnDestroy {

  private destroy$ = new Subject();

  constructor(
    private template: TemplateRef<any>,
    private view: ViewContainerRef,
    private store: Store<AppState>,
  ) {
  }

  ngOnInit(): void {
    this.store
      .pipe(
        select(getIsLogged),
        tap(() => this.view.clear()),
        // avoid to emit if isLogged does not change its value
        distinctUntilChanged(),
        // emit value only if isLogged
        filter(isLogged => !!isLogged),
        // unsubscribe when directive is destroyed
        takeUntil(this.destroy$)
      )
      .subscribe(() => this.view.createEmbeddedView(this.template));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }
}
