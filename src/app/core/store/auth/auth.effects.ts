// app/core/auth/auth.effects
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { of } from 'rxjs';
import { catchError, exhaustMap, filter, map, mapTo, tap } from 'rxjs/operators';
import { Actions, createEffect, ofType, ROOT_EFFECTS_INIT } from '@ngrx/effects';

import * as AuthActions from './auth.actions';
import { go } from '../router/router.actions';
import { Auth } from './auth';

@Injectable({ providedIn: 'root' })
export class AuthEffects {

  // automatically invoked when application starts
  initEffect$ = createEffect(() => this.actions$.pipe(
    ofType(ROOT_EFFECTS_INIT),
    // get token
    mapTo(JSON.parse(localStorage.getItem('auth')) as Auth),
    // we want dispatch an action only if the auth object exists
    filter(auth => !!auth),
    // save token in localStorage
    map(auth => AuthActions.syncWithLocalStorage({ auth }))
  ));

  // when login is invoked: invoke `login` endpoint and dispatch the success (or fail) action
  loginEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.login),
      exhaustMap((action) => {
        const params: HttpParams = new HttpParams()
          .set('email', action.email)
          .set('password', action.password);
        return this.http.get<Auth>('http://localhost:3000/login', { params }).pipe(
          map((auth: Auth) => AuthActions.loginSuccess({ auth })),
          catchError(() => of(AuthActions.loginFailed())),
        );
        }
      )
    )
  );

  loginSuccessEffect$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.loginSuccess),
      tap(action => {
        localStorage.setItem('auth', JSON.stringify(action.auth));
      }),
      mapTo(go({path: 'items'})),
    ),
  );

    logout$ = createEffect(() =>
      this.actions$.pipe(
        ofType(AuthActions.logout),
        tap(() => localStorage.removeItem('auth')),
        mapTo(go({path: 'login'}))
      )
    );

  constructor(
    private actions$: Actions,
    private http: HttpClient
  ) {
  }
}
