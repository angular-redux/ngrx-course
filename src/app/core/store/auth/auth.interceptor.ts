import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { iif, Observable, throwError } from 'rxjs';
import { catchError, mergeMap, take } from 'rxjs/operators';
import { getAuthToken } from './auth.selectors';
import { AppState } from '../../../app.module';
import { go } from '../router/router.actions';

@Injectable({ providedIn: 'root' })
export class AuthInterceptor implements HttpInterceptor {
  constructor(private store: Store<AppState>) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.store.select(getAuthToken).pipe(
      take(1),
      mergeMap(token => iif(
          () => !!token && req.url.includes('jsonplaceholder'),
          next.handle(req.clone({ setHeaders: { Authorization: 'Bearer ' + token }, })),
          next.handle(req)
        )
      ),
      // NEW
      catchError(err => {
        if (err instanceof HttpErrorResponse) {
          switch (err.status) {
            case 401:
              console.log('do something with 404 error', err);
              break;
            default:
            case 404:
              this.store.dispatch(go({ path: '/login'}));
              break;
          }
        }
        // return of(err);
        return throwError(err);
      }),

    );
  }
}
