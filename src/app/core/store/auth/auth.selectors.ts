import { AppState } from '../../../app.module';

export const getIsLogged = (state: AppState) =>  !!state.authentication.auth;
export const getAuthToken = (state: AppState) =>  state.authentication.auth?.token;
export const getAuthError = (state: AppState) =>  state.authentication.error;
